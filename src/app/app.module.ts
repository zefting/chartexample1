import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ChartsModule } from 'ng2-charts';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
// pages
import { AppComponent } from './app.component';
import { ChartsComponent } from './charts/charts.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SoldProductsListComponent } from './sold-products-list/sold-products-list.component';

@NgModule({
  declarations: [AppComponent, ChartsComponent, SoldProductsListComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    NgSelectModule,
    FormsModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
