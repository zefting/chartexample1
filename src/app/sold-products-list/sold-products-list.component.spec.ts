import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoldProductsListComponent } from './sold-products-list.component';

describe('SoldProductsListComponent', () => {
  let component: SoldProductsListComponent;
  let fixture: ComponentFixture<SoldProductsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoldProductsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoldProductsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
