import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sold-products-list',
  templateUrl: './sold-products-list.component.html',
  styleUrls: ['./sold-products-list.component.css'],
})
export class SoldProductsListComponent implements OnInit {
  // create an array to hold to incomming product-data
  products: any[];

  constructor() {}

  ngOnInit(): void {
    // Here NgOnInit is used to populate an array with some data to display in the html file, but in this method a call to a server, e.g. a REST service could be used
    this.products = [
      {
        id: 1,
        productName: 'Horse',
        price: 1000,
        costumerName: 'Denmark',
        costumerCity: 'Balle',
      },
      {
        id: 2,
        productName: 'Unihorn',
        price: 2000,
        costumerName: 'Denmark',
        costumerCity: 'Silkeborg',
      },
      {
        id: 3,
        productName: 'Zebra',
        price: 3000,
        costumerName: 'Germany',
        costumerCity: 'Berlin',
      },
      {
        id: 4,
        productName: 'Lapis',
        price: 500,
        costumerName: 'USA',
        costumerCity: 'New York',
      },
      {
        id: 5,
        productName: 'Postboxes',
        price: 400,
        costumerName: 'Spain',
        costumerCity: 'Madrid',
      },
    ];
  }
}
